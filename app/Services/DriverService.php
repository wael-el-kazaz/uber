<?php

namespace App\Services;

use App\Http\Repositories\DriverRepository;
use App\Http\Repositories\DriverTripRepository;
use App\Exceptions\UberException;
use App\Constants\Error;
use App\Constants\Driver;
use Carbon\Carbon;

class DriverService
{
    private $driverRepository;
    private $driverTripRepository;

    public function __construct(DriverRepository $driverRepository, DriverTripRepository $driverTripRepository) {
        $this->driverRepository = $driverRepository;
        $this->driverTripRepository = $driverTripRepository;
    }

    public function makeTrip($driverId) {
        $driverTrip = $this->driverTripRepository->createDriverTrip($driverId, uniqid());
        return $this-> addTripToDriverRecord($driverId);
    }

    private function addTripToDriverRecord($driverId) {
        $driver = $this->getById($driverId);
        $resetMonth = false;
        $resetYear = false;
        if (Carbon::now()->format("m") !== $driver->updated_at->format("m")) {
            $resetMonth = true;
            if (Carbon::now()->format("y") !== $driver->updated_at->format("y")) {
                $resetYear = true;
            }
        }
        return $this->driverRepository->incrementNumberOfTrips($driverId, $resetYear, $resetMonth);
    }

    private function getById($driverId) {
        $driver = $this->driverRepository->getById($driverId);
        if ($driver === null) {
            throw new UberException(Error::DRIVER_NOT_FOUND);
        }
        return $driver;
    }

    public function getTotalTripsInPeriod($period) {
        $startDate = $this->generateMonoloticStartDate($period);
        $endDate = $this->generateMonoloticEndDate($period);
        return $this->driverRepository->getTotalTripsInPeriod($startDate, $endDate, $period);
    }

    public function getMonolothicDrivers($period, $minumumTrips) {
        return $this->driverRepository->getMonolothicDrivers($period, $minumumTrips); 
    }

    private function generateMonoloticStartDate($period) {
        if ($period === Driver::MONOLOTHIC_PERIOD_YEAR) {
            return Carbon::now()->startOfYear();
        } else if ($period === Driver::MONOLOTHIC_PERIOD_MONTH) {
            return Carbon::now()->startOfMonth();
        }
        return null;
    }

    private function generateMonoloticEndDate($period) {
        if ($period ===  Driver::MONOLOTHIC_PERIOD_YEAR) {
            return Carbon::now()->endOfYear();
        } else if ($period === Driver::MONOLOTHIC_PERIOD_MONTH) {
            return Carbon::now()->endOfMonth();
        }
        return null;
    }
}
