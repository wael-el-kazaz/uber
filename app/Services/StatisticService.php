<?php

namespace App\Services;

use App\Services\DriverService;
use App\Exceptions\UberException;
use App\Constants\Driver;
use App\Constants\Error;

class StatisticService
{
    private $driverService;

    public function __construct(DriverService $driverService) {
        $this->driverService = $driverService;
    }

    public function getMonolothicDrivers($period) {
        $totalTrips = $this->driverService->getTotalTripsInPeriod($period);
        $minumumTrips = $totalTrips * Driver::MONOLOTHIC_PERCENTAGE;
        return $this->driverService->getMonolothicDrivers($period, $minumumTrips);
    }

}
