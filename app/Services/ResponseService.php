<?php

namespace App\Services;

use Auth;
use Config;
use App\Constants\Error;
use File;
use Image;
use Storage;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Illuminate\Http\Response;

class ResponseService {

    /**
     * Gets a response object with success status and the data returned to clients
     * 
     * @param mixed $data
     * @return Illuminate\Http\Response
     */
    public function getSuccessResponse($data = "") {
        $result = array('status' => 1, 'data' => $data);
        return response()->json($result);
    }
    
    /**
     * Gets a response object with maintenance status and the data returned to clients
     * 
     * @param mixed $data
     * @return Illuminate\Http\Response
     */
    public function getMaintenanceResponse($data = "") {
        $result = array('status' => 3, 'data' => $data);
        return response()->json($result, Response::HTTP_SERVICE_UNAVAILABLE);
    }

    /**
     * Gets a response object for any error that occurs through out the application
     * 
     * @param array $errorIdArr
     * @param int $responseCode
     * @param array $validationErrors
     * @param array $dataArr
     * @return Illuminate\Http\Response
     */
    public function getErrorResponse($errorId, $responseCode = Response::HTTP_OK, $msg, $validationErrors = null) {
        $result = array('status' => 0, 'error-code' => $errorId, "message" => $msg);
        if (is_array($validationErrors)) {
            $result['validation'] = $validationErrors;
        }
        return response()->json($result, $responseCode);
    }

}
