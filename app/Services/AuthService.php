<?php

namespace App\Services;

use JWTAuth;
use App\Http\Repositories\DriverRepository;
use App\Exceptions\UberException;
use App\Constants\Error;

class AuthService
{
    private $driverRepository;

    public function __construct(DriverRepository $driverRepository) {
        $this->driverRepository = $driverRepository;
    }

    public function authDriver(String $email, String $password) {
        $driver = $this->driverRepository->getDriverByEmail($email);
        if (is_null($driver) === true || !\Hash::check($password, $driver->password)) {
            throw new UberException(Error::DRIVER_NOT_FOUND);
        }

        $token = JWTAuth::fromUser($driver);
        return $token;
    }
}
