<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Auth\User as Authenticatable;

class DriverTrip extends Model
{
    protected $fillable = [
        "trip_code",
        "driver_id"
    ];   
}
