<?php

namespace App\Http\Repositories;

use App\Models\DriverTrip;

/**
 * This repository is for creating transactions history only
 * Avoid selecting from this repository as possible as the amount of data could be huge
 */
class DriverTripRepository
{
    public function createDriverTrip($driverId, $tripCode) {
        return DriverTrip::create([
                "trip_code" => $tripCode,
                "driver_id" => $driverId,
            ]);
    }
    
}
