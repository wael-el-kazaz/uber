<?php

namespace App\Http\Repositories;

use App\Models\Driver;
use App\Constants\Driver as DriverConstant;

class DriverRepository
{
    public function getDriverByEmail(String $email) {
        return Driver::where("email", $email)
            ->first();
    }

    public function incrementNumberOfTrips($driverId, $resetMonth, $resetYear) {

        $incrementFieldsArr = ['total_trips' => 1]; // an array for all the fields to be incremented
        $resetFieldsArr = []; // an array for all the fields to be reseted
        if ($resetMonth === false) {
            $incrementFieldsArr['month_trips'] = 1;
        } else {
            $resetFieldsArr['month_trips'] = 1;
        }
        if ($resetYear === false) {
            $incrementFieldsArr['year_trips'] = 1;
        } else {
            $resetFieldsArr['year_trips'] = 1;
        }

        return Driver::raw(function($collection) use ($driverId, $incrementFieldsArr, $resetFieldsArr){
            $modifiersArr = ['$inc' => $incrementFieldsArr];
            if (count($resetFieldsArr) > 0) {
                $modifiersArr['$set'] = $resetFieldsArr;
            }

            return $collection->findOneAndUpdate([
                '_id' => new \MongoDB\BSON\ObjectID($driverId),
            ], $modifiersArr);

        });

    }

    public function getById($driverId) {
        return Driver::find($driverId);
    }

    public function getTotalTripsInPeriod($startDate, $endDate, $period) {
        $criteriaField = $this->getStatisticsCriteriaField($period);
        
        $query = new Driver();
        if (isset($startDate) === true && isset($endDate) === true) {
            $query->whereDate("updated_at", ">=", $startDate)
                ->whereDate("updated_at", "<=", $endDate);
        }
        return $query->sum($criteriaField);
    }

    public function getMonolothicDrivers($period, $minumumTrips) {
        $criteriaField = $this->getStatisticsCriteriaField($period);
        return Driver::where($criteriaField, ">=", $minumumTrips)
            ->orderBy($criteriaField, "desc")
            ->get(); // no need for pagination as the current minimum trips variable will limit the number of drivers eg at most drivers count (minimumTrips * 100/ totalTrips)
    }

    private function getStatisticsCriteriaField($period) {
        if ($period ===  DriverConstant::MONOLOTHIC_PERIOD_YEAR) {
            return "year_trips";
        } else if ($period === DriverConstant::MONOLOTHIC_PERIOD_MONTH) {
            return "month_trips";
        } else {
            return "total_trips";
        }
    }

}
