<?php

namespace App\Constants;

use Illuminate\Http\Response;

class Error {

    const LOW_SEVERITY = 1;
    const MEDIUM_SEVERITY = 2;
    const HIGH_SEVERITY = 3;
    
    const SEVERITY_MSG = array(
        self::LOW_SEVERITY => "Low",
        self::MEDIUM_SEVERITY => "Medium",
        self::HIGH_SEVERITY => "High",
    );
    
    const UNKNOWN_ERROR = 0;
    const DRIVER_NOT_FOUND = 1;
    const INVALID_INPUTS = 2;
    const TOKEN_INVALID = 3;
    const TOKEN_EXPIRED = 4;
    const NOT_AUTHENTICATED = 5;
    

    const MSG = array(
        self::UNKNOWN_ERROR => 'Unknown Error',
        self::DRIVER_NOT_FOUND => 'Driver not found',
        self::INVALID_INPUTS => 'Invalid Inputs',
        self::TOKEN_INVALID => 'Invalid token',
        self::TOKEN_EXPIRED => 'Token expired',
        self::NOT_AUTHENTICATED => 'Not authenticated',
    );

    const HTTPCODE = array(
        self::UNKNOWN_ERROR => Response::HTTP_INTERNAL_SERVER_ERROR,
        self::DRIVER_NOT_FOUND => Response::HTTP_NOT_FOUND,
        self::INVALID_INPUTS => Response::HTTP_UNPROCESSABLE_ENTITY,
        self::TOKEN_INVALID => Response::HTTP_UNAUTHORIZED,
        self::TOKEN_EXPIRED => Response::HTTP_UNAUTHORIZED,
        self::NOT_AUTHENTICATED => Response::HTTP_UNAUTHORIZED,
    );

    const SEVERITY = array(
        self::UNKNOWN_ERROR => self::HIGH_SEVERITY,
        self::DRIVER_NOT_FOUND => self::MEDIUM_SEVERITY,
        self::TOKEN_INVALID => self::MEDIUM_SEVERITY,
        self::TOKEN_EXPIRED => self::MEDIUM_SEVERITY,
        self::NOT_AUTHENTICATED => self::MEDIUM_SEVERITY,
    );

}
