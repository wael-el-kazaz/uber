<?php

namespace App\Constants;

use Illuminate\Http\Response;

class Driver {

    const MONOLOTHIC_PERIOD_YEAR = "year";
    const MONOLOTHIC_PERIOD_MONTH = "month";
    const MONOLOTHIC_PERCENTAGE = 10 / 100;

}
