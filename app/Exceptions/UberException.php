<?php

namespace App\Exceptions;

class UberException extends \Exception {

    public function __construct($code, $dataArr = array()) {
        $this->httpCode = \App\Constants\Error::HTTPCODE[$code];
        $this->severity = \App\Constants\Error::SEVERITY[$code];
        parent::__construct(\App\Constants\Error::MSG[$code], $code);
    }

    private $httpCode;
    private $severity;

    public function getHttpCode() {
        return $this->httpCode;
    }

    public function getSeverity() {
        return $this->severity;
    }

}
