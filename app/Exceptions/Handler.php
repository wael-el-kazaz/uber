<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Exceptions\UberException;
use App\Services\ResponseService;
use App\Constants\Error;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $responseService = new ResponseService();
        if (is_a($exception, 'App\Exceptions\UberException')) {
            return $responseService->getErrorResponse($exception->getCode(), $exception->getHttpCode(), $exception->getMessage());
        } else if (is_a($exception, 'Illuminate\Validation\ValidationException')) {
            return $responseService->getErrorResponse(Error::INVALID_INPUTS, Error::HTTPCODE[Error::INVALID_INPUTS], Error::MSG[Error::INVALID_INPUTS] ,$exception->validator->errors()->messages());
        } elseif (is_a($exception, 'Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException')) {
            if (is_a($exception->getPrevious(), 'Tymon\JWTAuth\Exceptions\TokenInvalidException')) {
                throw new UberException(Error::TOKEN_INVALID);
            } elseif (is_a($exception->getPrevious(), 'Tymon\JWTAuth\Exceptions\TokenExpiredException')) {
                throw new UberException(Error::TOKEN_EXPIRED);
            }
            throw new UberException(Error::NOT_AUTHENTICATED);
        } else {
            throw new UberException(Error::UNKNOWN_ERROR);
        } 
    }
}
