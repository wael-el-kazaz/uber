<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use App\Http\Requests\AuthRequest;
use App\Http\Requests\GetMonolothicDriversRequest;
use App\Services\AuthService;
use App\Services\DriverService;
use App\Services\ResponseService;

class DriverController extends Controller
{
    private $authService;
    private $driverService;
    private $responseService;

    public function __construct(ResponseService $responseService, AuthService $authService, DriverService $driverService) {
        $this->authService = $authService;
        $this->responseService = $responseService;
        $this->driverService = $driverService;
    }

    public function auth(AuthRequest $request) {
        $token = $this->authService->authDriver($request->input("email"), $request->input("password"));
        return $this->responseService->getSuccessResponse($token);
    }

    public function makeTrip() {
        $this->driverService->makeTrip(\Auth::user()->id);
        return $this->responseService->getSuccessResponse(true);
    }

    public function getMonolothic(GetMonolothicDriversRequest $request) {
        $result = $this->driverService->getMonolothicDrivers($request->input("period"));
        return $this->responseService->getSuccessResponse($result);
    }
}
