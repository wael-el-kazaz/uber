<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GetMonolothicDriversRequest;
use App\Services\StatisticService;
use App\Services\ResponseService;

class StatisticController extends Controller
{
    public function __construct(ResponseService $responseService, StatisticService $statisticService) {
        $this->statisticService = $statisticService;
        $this->responseService = $responseService;
    }

    public function getMonolothic(GetMonolothicDriversRequest $request) {
        $result = $this->statisticService->getMonolothicDrivers($request->input("period"));
        return $this->responseService->getSuccessResponse($result);
    }
}
