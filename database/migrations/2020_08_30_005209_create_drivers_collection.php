<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriversCollection extends Migration
{
    /**
     * the name of the collection to be created
     */
    private $collectionName = 'drivers';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create($this->collectionName, function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->index();
            $table->string('password');
            $table->integer('month_trips')->default(0)->comment('the number of current month trips');
            $table->integer('year_trips')->default(0)->comment('the number of today trips');
            $table->integer('total_trips')->default(0)->comment('the number of current year trips');
            $table->timestamps();
        });
            
            \DB::raw("db.." . $this->collectionName .".createIndex({month_trips: -1})");
            \DB::raw("db." . $this->collectionName .".createIndex({year_trips: -1})");
            \DB::raw("db.." . $this->collectionName .".createIndex({total_trips: -1})");
            \DB::raw("db.." . $this->collectionName .".createIndex()");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->collectionName);
    }
}
