<?php

use Illuminate\Database\Seeder;
use App\Models\Driver;

class DriverSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i<=10;$i++) {
            Driver::create([
                "name" => "Driver$i",
                "email" => "driver$i@uber.com",
                "password" => Hash::make("driver$i"),
                "month_trips" => 0,
                "year_trips" => 0,
                "total_trips" => 0,
            ]);
        }
        
    }
}
